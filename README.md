
# Using the app Locally
1. Run `python3 manage.py makemigrations`
2. Run `python3 manage.py migrate`
3. Run `python3 manage.py sql` to dump json data into database
4. Run `python3 manage.py test` to run test suite

# Documentation

Postman documentation is available at https://documenter.getpostman.com/view/11396719/Tzm9kaab

# Exposed endpoints

1. https://passthekey.herokuapp.com/api/nexus/{{outcode}}/
2. https://passthekey.herokuapp.com/api/nexus/{{outcode}}/
