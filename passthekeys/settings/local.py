from .base_settings import *
from dj_database_url import parse as db_url

DATABASES = {
   'default': config('DATABASE_URL', default='postgres:///postcode', cast=db_url),
}