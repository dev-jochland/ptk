import requests

from api.constants import POSTCODE_URL


def get_nearest_outcode(outcode):
    nearest_outcodes_url = '{}/{}/nearest'.format(POSTCODE_URL, outcode).capitalize()
    response = requests.get(nearest_outcodes_url)
    return response.json()