from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from rest_framework.routers import DefaultRouter

from .views import ListingView, NexusView

router = DefaultRouter(trailing_slash=False)
app_router = routers.DefaultRouter()

app_router.register('outcode', ListingView, basename='outcode')
app_router.register('nexus', NexusView, basename='nexus')

urlpatterns = [
    path('', include(app_router.urls))
]
