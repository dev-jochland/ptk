import math

radius_of_earth = 6373.0


def calculate_distance_between_two_coordinates(latitude_1, longitude_1, latitude_2, longitude_2):
    lat1 = math.radians(latitude_1)
    lon1 = math.radians(longitude_1)
    lat2 = math.radians(latitude_2)
    lon2 = math.radians(longitude_2)

    difference_in_longitude = lon2 - lon1
    difference_in_latitude = lat2 - lat1

    a = math.pow(math.sin(difference_in_latitude/2), 2) + math.cos(lat1) * math.cos(lat2) * \
        math.pow(math.sin(difference_in_longitude/2), 2)

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = radius_of_earth * c
    return '{:.2f}'.format(distance)


def next_value(distance):
    i = 0
    while i < len(distance):
        yield distance[i]
        i += 1
