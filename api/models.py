from django.db import models


class Listings(models.Model):
    longitude = models.CharField(max_length=15)
    latitude = models.CharField(max_length=15)
    weekly_price = models.DecimalField(decimal_places=2, max_digits=13)
    out_code = models.CharField(null=True, blank=True, max_length=10)

    def __str__(self):
        return 'lon: {}, lat: {}'.format(self.longitude, self.latitude)

    def __repr__(self):
        return 'lon: {}, lat: {}'.format(self.longitude, self.latitude)
