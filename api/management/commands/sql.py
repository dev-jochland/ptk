import json
from django.core.management.base import BaseCommand
from api.models import Listings


class Command(BaseCommand):
    def handle(self, *args, **options):
        # save_csv_data_to_database()
        with open('data_file.json') as data:
            data_list = json.load(data)

        Listings.objects.all().delete()
        for data in data_list:
            Listings.objects.create(longitude=data['longitude'], latitude=data['latitude'], weekly_price=data[
                'weekly_price'], out_code=data['out_code'])
        return 'done uploading'
