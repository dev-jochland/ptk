import json

from requests.exceptions import SSLError, ConnectionError
from rest_framework.response import Response
from rest_framework_xml.parsers import XMLParser

from .models import Listings
from .renderer import MyCustomXMLRenderer
from .serializers import ListingsSerializer, SingularOutCodeSerializer, AllNexusOutCodeSerializers
from rest_framework import status, viewsets

from .service import get_nearest_outcode
from .utils import calculate_distance_between_two_coordinates

"""
def cal(root):
    for child in root:
        if child[0].tag == "nexus":
            for i in range(len(root.find('outcode'))):
                tag = root[0][i].tag
                text = root[0][i].text
                root.set(tag, text)
        else:
            for i in range(len(child)):
                tag = child[i].tag
                text = child[i].text
                child.set(tag, text)
    for parent in root.findall('.//outcode'):
        for element in parent.findall('listing_count'):
            parent.remove(element)
        for element in parent.findall('out_code'):
            parent.remove(element)
        for element in parent.findall('average_daily_rate'):
            parent.remove(element)
    root.remove(root.find('outcode'))
    return

"""


class ListingView(viewsets.ModelViewSet):
    queryset = Listings.objects.all()
    serializer_class = ListingsSerializer
    parser_classes = (XMLParser,)
    renderer_classes = (MyCustomXMLRenderer,)
    lookup_url_kwarg = 'out_code'

    def retrieve(self, request, *args, **kwargs):
        try:
            queryset = Listings.objects.filter(out_code=self.kwargs.get('out_code').capitalize()).first()
            if not isinstance(queryset, type(None)):
                serializer = ListingsSerializer(queryset, context={'out_code': self.kwargs.get('out_code')})
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(status=status.HTTP_404_NOT_FOUND)
        except ZeroDivisionError:
            return 0
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)


class NexusView(viewsets.ViewSet):
    lookup_url_kwarg = 'out_code'
    parser_classes = (XMLParser,)
    renderer_classes = (MyCustomXMLRenderer,)

    def retrieve(self, request, *args, **kwargs):
        try:
            queryset = Listings.objects.filter(out_code=self.kwargs.get('out_code').capitalize()).first()
            if not isinstance(queryset, type(None)):
                response_data_for_nearest_out_codes = get_nearest_outcode(self.kwargs.get('out_code'))
                out_code_list = [(out_code['outcode']) for out_code in response_data_for_nearest_out_codes.get(
                    'result')]
                out_code_serializer_query = Listings.objects.filter(out_code__in=out_code_list).distinct('out_code')

                location_data = [({"out_code": element["outcode"], "longitude": element["longitude"],
                                   "latitude": element["latitude"]}) for element in
                                 response_data_for_nearest_out_codes.get('result')]
                base_latitude = location_data[0]['latitude']
                base_longitude = location_data[0]['longitude']
                calculate_distance = [calculate_distance_between_two_coordinates(base_latitude, base_longitude,
                                                                                 element['latitude'],
                                                                                 element['longitude']) for element in
                                      location_data]
                all_nexus_serializer = AllNexusOutCodeSerializers(queryset,
                                                                  context={
                                                                      'nexus_coordinates': location_data})
                out_code_serializer = SingularOutCodeSerializer(out_code_serializer_query, many=True, context={
                    'nexus_coordinates': calculate_distance})

                data_1 = json.dumps(all_nexus_serializer.data)  # serializer data is immutable, and I needed to
                # manipulate it.
                data_2 = json.dumps(out_code_serializer.data)
                output_data = [json.loads(data_1)]
                [output_data.append(element) for element in json.loads(data_2)]
                return Response(output_data, status=status.HTTP_200_OK)
            return Response(status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response(status.HTTP_404_NOT_FOUND)
        except TypeError:
            Response(status.HTTP_404_NOT_FOUND)
        except (SSLError, ConnectionError) as e:
            return Response(str(e), status=status.HTTP_503_SERVICE_UNAVAILABLE)
