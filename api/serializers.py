from django.db.models import Avg
from rest_framework import serializers

from api.models import Listings


class ListingsSerializer(serializers.ModelSerializer):
    listing_count = serializers.SerializerMethodField()
    average_daily_rate = serializers.SerializerMethodField()
    out_code = serializers.SerializerMethodField()

    class Meta:
        model = Listings
        fields = ('listing_count', 'average_daily_rate', 'out_code')

    def get_listing_count(self, obj) -> int:
        listing_count = Listings.objects.filter(out_code=obj.out_code).count()
        return listing_count

    def get_average_daily_rate(self, obj) -> float:
        average_weekly_price = Listings.objects.filter(out_code=obj.out_code).aggregate(Avg('weekly_price'))
        average_daily_rate = float('{:.2f}'.format(average_weekly_price['weekly_price__avg'] / 7))
        return average_daily_rate

    def get_out_code(self, obj) -> str:
        return self.context('out_code')


class SingularOutCodeSerializer(serializers.ModelSerializer):
    """serializer data for each nexus out_code"""
    listing_count = serializers.SerializerMethodField()
    average_daily_rate = serializers.SerializerMethodField()
    distance_from_nexus = serializers.SerializerMethodField()

    class Meta:
        model = Listings
        fields = ('out_code', 'listing_count', 'average_daily_rate', 'distance_from_nexus')

    def get_listing_count(self, obj) -> int:
        listing_count = Listings.objects.filter(out_code=obj.out_code).count()
        return listing_count

    def get_average_daily_rate(self, obj) -> float:
        try:
            out_code_weekly_average = Listings.objects.filter(out_code=obj.out_code).aggregate(Avg('weekly_price'))
            out_code_average_daily_rate = float('{:.2f}'.format(out_code_weekly_average['weekly_price__avg'] / 7))
            return out_code_average_daily_rate
        except KeyError:
            return 0

    def get_distance_from_nexus(self, obj) -> float:
        nexus_coordinate_data = self.context.get('nexus_coordinates')
        distance = nexus_coordinate_data.pop(0)
        return distance


class AllNexusOutCodeSerializers(serializers.ModelSerializer):
    """Serializer for aggregated data for all out_codes in nexus out_code"""
    nexus = serializers.SerializerMethodField()
    listing_count = serializers.SerializerMethodField()
    average_daily_rate = serializers.SerializerMethodField()

    class Meta:
        model = Listings
        fields = ('nexus', 'listing_count', 'average_daily_rate')

    def get_nexus(self, obj):
        out_code = obj.out_code
        return out_code

    def get_listing_count(self, obj):
        nexus_coordinate_data = self.context.get('nexus_coordinates')
        sum_all_listings = [(Listings.objects.filter(out_code=element['out_code']).count()) for element in
                            nexus_coordinate_data]
        return sum(sum_all_listings)

    def get_average_daily_rate(self, obj):
        try:
            nexus_coordinate_data = self.context.get('nexus_coordinates')
            all_listings_average_weekly_rate = []

            for element in nexus_coordinate_data:
                k = Listings.objects.filter(out_code=element['out_code']).aggregate(Avg(
                    'weekly_price'))
                p = k['weekly_price__avg'] / 7
                all_listings_average_weekly_rate.append(p)
            average_daily_rate = float(
                '{:.2f}'.format(sum(all_listings_average_weekly_rate) / len(nexus_coordinate_data)))
            return average_daily_rate
        except (TypeError, ZeroDivisionError):
            return 0
