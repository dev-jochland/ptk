from django.utils.encoding import force_str
from rest_framework_xml.renderers import XMLRenderer
from django.utils.xmlutils import SimplerXMLGenerator


from io import StringIO



class MyCustomXMLRenderer(XMLRenderer):
    """overwrite here to return the desired xml format"""
    root_tag_name = ''
    item_tag_name = 'outcode'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders `data` into serialized XML.
        """
        if data is None:
            return ""

        stream = StringIO()

        xml = SimplerXMLGenerator(stream, self.charset)
        xml.startDocument()
        # xml.startElement(self.root_tag_name, {})

        self._to_xml(xml, data)

        # xml.endElement(self.root_tag_name)
        xml.endDocument()
        return stream.getvalue()

    def _to_xml(self, xml, data):
        if isinstance(data, (list, tuple)):
            x = {}
            for item in data:
                if item.get("nexus"):
                    for key, value in item.items():
                        x[key] = str(value)
                    xml.startElement("outcodes", x)
                else:
                    self._to_xml(xml, item)
            xml.endElement("outcodes")

        elif isinstance(data, dict):
            x = {}
            for key, value in data.items():
                if key == "out_code":
                    continue
                x[key] = str(value)
            xml.startElement(self.item_tag_name, x)
            self._to_xml(xml, data.get("out_code"))
            xml.endElement("outcode")

        elif data is None:
            # Don't output any value
            pass

        else:
            xml.characters(force_str(data))
