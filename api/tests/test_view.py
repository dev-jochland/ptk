from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

import api.models as am
import api.serializers as aps


class ListingViewsetCRUD(TestCase):
    """Test module for the listing viewset"""

    def setUp(self):
        self.client = APIClient()

        am.Listings.objects.create(
            longitude="117.0",
            latitude="54.0",
            weekly_price=235.6,
            out_code="M19"
        )

    def test_read(self):
        # read single
        single_listing = am.Listings.objects.get(out_code="M19")
        response = self.client.get(reverse("outcode-detail", kwargs={'out_code': single_listing.out_code}))
        serializer = aps.ListingsSerializer(single_listing, many=False)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

        # read no data
        response = self.client.get(reverse("outcode-detail", kwargs={'out_code': "M80"}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class NexusViewset(TestCase):
    """Test module for the nexus viewset"""

    def setUp(self):
        self.client = APIClient()

        test_listing_1 = am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M12"
        )
        test_listing_1 = am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M20"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M21"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M22"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M13"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M14"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M15"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M16"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=235.9,
            out_code="M17"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=234.9,
            out_code="M18"
        )
        am.Listings.objects.create(
            longitude="127.0",
            latitude="54.4",
            weekly_price=23.9,
            out_code="M19"
        )
        am.Listings.objects.create(
            longitude="117.0",
            latitude="54.0",
            weekly_price=235.6,
            out_code="M19"
        )

    # def test_read(self):
    #     # read multiple
    #     response = self.client.get(reverse("nexus-list"))
    #     all_listing = am.Listings.objects.filter(out_code="M19")
    #     serializer = aps.ListingsSerializer(all_listing, many=True)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data, serializer.data)
