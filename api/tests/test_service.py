from unittest import TestCase
from unittest.mock import Mock, patch
from rest_framework.test import APIClient

from nose.tools import assert_dict_equal

from api.service import get_nearest_outcode


class TestService(TestCase):
    """Test module for the listing viewset"""

    def __init__(self, methodName: str):
        super().__init__(methodName)
        self.mock_get = None

    def setUp(self):
        self.client = APIClient()

    @patch('api.service.requests.get')
    def test_get_postcode_data(self, mock_get):
        data = {
            "status": 200,
            "result": [
                {
                    "outcode": "SK8",
                    "longitude": -2.2090867702192,
                    "latitude": 53.3796819213907,
                    "northings": 387002,
                    "eastings": 386189,
                    "admin_district": [
                        "Stockport",
                        "Cheshire East"
                    ],
                    "parish": [
                        "Handforth",
                        "Stockport, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Davenport and Cale Green",
                        "Heald Green",
                        "Handforth",
                        "Cheadle Hulme South",
                        "Bramhall South and Woodford",
                        "Cheadle Hulme North",
                        "Cheadle and Gatley"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "SK3",
                    "longitude": -2.1721984431631,
                    "latitude": 53.3982718995058,
                    "northings": 389063,
                    "eastings": 388647,
                    "admin_district": [
                        "Stockport"
                    ],
                    "parish": [
                        "Stockport, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Stepping Hill",
                        "Cheadle Hulme North",
                        "Edgeley and Cheadle Heath",
                        "Davenport and Cale Green",
                        "Brinnington and Central"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "M22",
                    "longitude": -2.26008094905506,
                    "latitude": 53.3858656359901,
                    "northings": 387701,
                    "eastings": 382799,
                    "admin_district": [
                        "Stockport",
                        "Cheshire East",
                        "Manchester"
                    ],
                    "parish": [
                        "Stockport, unparished area",
                        "Styal",
                        "Manchester, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Wilmslow Lacey Green",
                        "Sharston",
                        "Northenden",
                        "Cheadle and Gatley",
                        "Woodhouse Park"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "SK7",
                    "longitude": -2.14340607242063,
                    "latitude": 53.3695112470238,
                    "northings": 385859,
                    "eastings": 390555,
                    "admin_district": [
                        "Stockport",
                        "Cheshire East"
                    ],
                    "parish": [
                        "Handforth",
                        "Stockport, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Handforth",
                        "Hazel Grove",
                        "Bramhall North",
                        "Stepping Hill",
                        "Brinnington and Central",
                        "Bramhall South and Woodford",
                        "Marple South and High Lane"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "SK4",
                    "longitude": -2.18172340246914,
                    "latitude": 53.4194618283951,
                    "northings": 391422,
                    "eastings": 388020,
                    "admin_district": [
                        "Stockport",
                        "Manchester"
                    ],
                    "parish": [
                        "Stockport, unparished area",
                        "Manchester, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Heatons South",
                        "Reddish South",
                        "Didsbury East",
                        "Heatons North",
                        "Brinnington and Central"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "SK1",
                    "longitude": -2.15155822222222,
                    "latitude": 53.4076046525253,
                    "northings": 390098,
                    "eastings": 390022,
                    "admin_district": [
                        "Stockport"
                    ],
                    "parish": [
                        "Stockport, unparished area"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Brinnington and Central",
                        "Manor"
                    ],
                    "country": [
                        "England"
                    ]
                },
                {
                    "outcode": "M90",
                    "longitude": -2.27807078823529,
                    "latitude": 53.3635075294118,
                    "northings": 385218,
                    "eastings": 381593,
                    "admin_district": [
                        "Manchester"
                    ],
                    "parish": [
                        "Manchester, unparished area",
                        "Ringway"
                    ],
                    "admin_county": [],
                    "admin_ward": [
                        "Northenden",
                        "Woodhouse Park"
                    ],
                    "country": [
                        "England"
                    ]
                }
            ]
        }
        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.json.return_value = data

        response = get_nearest_outcode("M12")

        assert_dict_equal(response, data)
